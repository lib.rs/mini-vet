#![doc = include_str!("../README.md")]

use std::borrow::Cow;
#[doc(hidden)]
pub use ahash::HashMap;
#[doc(hidden)]
pub use semver::Version as SemVer;
#[doc(hidden)]
pub use semver::VersionReq;
#[doc(hidden)]
pub use unicase::Ascii;

use futures::future::join_all;
use log::debug;
use quick_error::quick_error;
use std::collections::hash_map::Entry;
use std::fmt;
use std::time::Duration;

pub const DEFAULT_REGISTRY_URL: &str = "https://raw.githubusercontent.com/bholley/cargo-vet/main/registry.toml";

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        /// HTTP request failed
        Req(e: reqwest::Error) {
            display("Error fetching vet")
            source(e)
            from()
        }
        /// Parsing failed
        Toml(e: Box<toml::de::Error>) {
            display("Error parsing vet")
            source(&**e)
            from(e: toml::de::Error) -> (Box::new(e))
        }
        Other(msg: Box<str>) {
            display("{}", msg)
        }
    }
}

/// **Start here**
pub struct MiniVet<'client> {
    client: Cow<'client, reqwest::Client>,
}

/// Registry entry for data sources
#[derive(Debug, Clone, serde::Deserialize)]
pub struct AuditsUrl {
    #[serde(deserialize_with = "maybe_vec")]
    pub url: Vec<String>,
}

/// A list of URLs to fetch. See the default registry for the sources: <https://raw.githubusercontent.com/bholley/cargo-vet/main/registry.toml>.
#[derive(Debug, Clone, serde::Deserialize)]
pub struct Registry {
    pub registry: HashMap<String, AuditsUrl>,
}

/// cargo-vet makes most fields flexible and allows either a single item as a string, or a list of stirngs
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(untagged)]
enum StrOrVec {
    Vec(Vec<String>),
    Str(String),
}

impl Default for StrOrVec {
    fn default() -> Self {
        Self::Vec(Vec::new())
    }
}

impl AsRef<[String]> for StrOrVec {
    #[inline]
    fn as_ref(&self) -> &[String] {
        match self {
            Self::Str(s) => std::slice::from_ref(s),
            Self::Vec(v) => v,
        }
    }
}

impl From<StrOrVec> for Vec<String> {
    #[inline]
    fn from(s: StrOrVec) -> Vec<String> {
        match s {
            StrOrVec::Str(s) => vec![s],
            StrOrVec::Vec(v) => v,
        }
    }
}

impl AsMut<Vec<String>> for StrOrVec {
    fn as_mut(&mut self) -> &mut Vec<String> {
        match self {
            Self::Str(_) => {
                let val = std::mem::replace(self, StrOrVec::Str(String::new()));
                *self = StrOrVec::Vec(vec![match val {
                    StrOrVec::Str(s) => s,
                    _ => unreachable!(),
                }]);
                match self {
                    StrOrVec::Vec(v) => v,
                    _ => unreachable!(),
                }
            },
            Self::Vec(v) => v,
        }
    }
}

/// Unfortunately, cargo-vet sometimes exposes internal IDs of crates.io users
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(untagged)]
pub enum StrOrNum {
    Str(String),
    Num(i64),
}

/// cargo-vet allows specifying git revisions for versions, but presence of the revision
/// seems to imply that the crate is not available on crates.io
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct VetVersionRef<'a> {
    pub version: &'a str,
    pub git_rev: Option<&'a str>,
}

/// Looks for `@git:` in the version
impl<'a> VetVersionRef<'a> {
    fn new(s: &'a str) -> Self {
        let (version, git_rev) = s.split_once("@git:").map(|(v, g)| (v, Some(g))).unwrap_or((s, None));
        Self { version, git_rev }
    }
}

impl fmt::Display for VetVersionRef<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.version)?;
        if let Some(rev) = self.git_rev {
            f.write_str("@git:")?;
            f.write_str(rev)?;
        }
        Ok(())
    }
}

/// A record of a review. If `violation` is not set, it's an approval, but check the `criteria` to know what has been approved.
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Audit {
    /// May be empty if `delta` is set
    pub version: Option<String>,
    /// If set, it's review of a diff.
    ///
    /// The syntax is `version_a -> version_b`, where the diff between `version_a` and `version_b` was audited.
    ///
    /// NB: `from`/`to` may be reversed, and that is allowed! It means review of an older version.
    pub delta: Option<String>,

    /// `VersionReq` doesn't play nice with pre-release versions, so be careful about those.
    pub violation: Option<VersionReq>,

    /// Look up in the `AuditFile`
    #[serde(deserialize_with = "maybe_vec")]
    pub criteria: Vec<String>,

    /// A string identifying the auditor.
    ///
    /// See [cargo_author](https://lib.rs/crates/cargo_author) for parsing.
    #[serde(default, deserialize_with = "maybe_vec")]
    pub who: Vec<String>,

    /// An optional free-form string containing any information the auditor may wish to record.
    pub notes: Option<String>,

    /// Reviews may be copied from different URLs
    #[serde(default, deserialize_with = "maybe_vec")]
    pub aggregated_from: Vec<String>,
}

impl Audit {
    /// make a key with all semantically important fields except criteria
    fn review_key_without_criteria(&self, crate_name: &str) -> String {
        let mut w = self.who.iter().map(|s| s.as_str()).collect::<Vec<_>>();
        w.sort_unstable();
        let mut k = w.join("|");
        k.try_reserve(100 + crate_name.len()).unwrap();
        k.push_str(crate_name);
        k.push(';');
        k.push_str(self.delta.as_deref().unwrap_or_default());
        k.push_str(self.version.as_deref().unwrap_or_default());
        k.push('!');
        k.push_str(&self.violation.as_ref().map(|v| v.to_string()).unwrap_or_default());
        k.push('+');
        k.push_str(self.notes.as_deref().unwrap_or_default());
        k
    }

    /// Parse version for diff reviews
    #[must_use]
    pub fn delta(&self) -> Option<(VetVersionRef<'_>, VetVersionRef<'_>)> {
        self.delta.as_deref().and_then(|d| {
            let (from, to) = d.split_once("->")?;
            Some((VetVersionRef::new(from.trim_end()), VetVersionRef::new(to.trim_start())))
        })
    }
}

/// Approved without checking
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct WildcardAudit {
    #[serde(default, deserialize_with = "maybe_vec")]
    pub who: Vec<String>,

    /// Specifies the relevant criteria for this wildcard audit.
    #[serde(deserialize_with = "maybe_vec")]
    pub criteria: Vec<String>,

    /// May contain a private crates.io user ID
    pub user_id: StrOrNum,
    /// Crates published by the user before this date will not be considered as certified
    pub start: String,
    /// Crates published by the user after this date will not be considered as certified
    pub end: String,
    /// Specifies whether cargo vet check should suggest renewal for this audit
    #[serde(default)]
    pub renew: bool,
    pub notes: Option<String>,

    /// Reviews may be copied from different URLs
    #[serde(default, deserialize_with = "maybe_vec")]
    pub aggregated_from: Vec<String>,
}

/// Approval of everything by a user, without checking
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Trusted {
    #[serde(deserialize_with = "maybe_vec")]
    pub criteria: Vec<String>,

    /// A private crates.io user ID
    pub user_id: StrOrNum,

    /// Crates published by the user before this date will not be considered as certified
    pub start: String,
    /// Crates published by the user after this date will not be considered as certified
    pub end: String,

    pub notes: Option<String>,

    /// Officially this doesn't exist, but seems like it should?
    #[doc(hidden)]
    #[serde(default, deserialize_with = "maybe_vec")]
    pub who: Vec<String>,

    /// Reviews may be copied from different URLs
    #[serde(default, deserialize_with = "maybe_vec")]
    pub aggregated_from: Vec<String>,
}

/// `safe-to-run` and `safe-to-deploy` are two special ones, meaning "no malware" and "no dangerous bugs", respectively.
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Criterion {
    pub description: Option<String>,
    pub description_url: Option<String>,
    /// This criterion is also met. Meaning of this when there's a violation is… weird.
    #[serde(default, deserialize_with = "maybe_vec")]
    pub implies: Vec<String>,

    /// Reviews may be copied from different URLs
    #[serde(default, deserialize_with = "maybe_vec")]
    pub aggregated_from: Vec<String>,
}

/// Case-insensitive string
#[derive(Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd, serde::Deserialize)]
#[serde(transparent)]
#[repr(transparent)]
pub struct CrateName(
    #[serde(deserialize_with = "u")]
    pub Ascii<String>
);

impl std::ops::Deref for CrateName {
    type Target = str;
    fn deref(&self) -> &str {
        &self.0
    }
}

impl<'a> std::borrow::Borrow<Ascii<String>> for CrateName {
    fn borrow(&self) -> &Ascii<String> {
        &self.0
    }
}

/// A file containing criteria and audits
#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct AuditsFile {
    /// What can be approved here
    #[serde(default)]
    pub criteria: HashMap<String, Criterion>,

    /// Acutual code reviews
    #[serde(default)]
    pub audits: HashMap<CrateName, Vec<Audit>>,

    /// Approved without checking
    #[serde(default)]
    pub wildcard_audits: HashMap<CrateName, Vec<WildcardAudit>>,

    /// Approved without checking
    #[serde(default)]
    pub trusted: HashMap<CrateName, Vec<Trusted>>,
}

fn u<'de, D>(deserializer: D) -> Result<Ascii<String>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let value: String = serde::Deserialize::deserialize(deserializer)?;
    Ok(Ascii::new(value))
}

fn maybe_vec<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let value: StrOrVec = serde::Deserialize::deserialize(deserializer)?;
    Ok(match value {
        StrOrVec::Str(s) => vec![s],
        StrOrVec::Vec(v) => v,
    })
}

/// A fetched and parsed list of reviews
#[derive(Debug, Clone)]
pub struct AuditSource {
    /// Short nickname
    pub name: String,
    /// Canonical URL
    pub url: String,
    /// Content
    pub audits: AuditsFile,
}

impl AuditSource {
    #[must_use]
    pub fn view_url(&self) -> &str {
        // weird google repo
        self.url.strip_suffix("?format=TEXT").unwrap_or(&self.url)
    }

    #[must_use] pub fn host(&self) -> &str {
        self.url.strip_prefix("https://").unwrap_or(&self.url).split('/').next().unwrap()
    }

    /// username, repo
    #[must_use] pub fn github_repo(&self) -> Option<(&str, &str)> {
        let mut parts = self.url
            .strip_prefix("https://raw.githubusercontent.com/")?
            .split('/');
        Some((parts.next()?, parts.next()?))
    }
}

/// A reference to an [`Audit`].
#[derive(Debug, Clone)]
pub struct Review<'a> {
    pub crate_name: &'a str,
    pub src: &'a AuditSource,
    pub audit: AuditKind<'a>,
}

impl<'a> Review<'a> {
    /// What is the scope of this audit
    #[must_use]
    pub fn criteria(&self) -> &[String] {
        match self.audit {
            AuditKind::Audit(a) => a.criteria.as_ref(),
            AuditKind::WildcardAudit(a) => a.criteria.as_ref(),
            AuditKind::Trusted(a) => a.criteria.as_ref(),
        }
    }

    /// List of URLs where this review originally appeared
    #[must_use]
    pub fn aggregated_from(&self) -> &[String] {
        match self.audit {
            AuditKind::Audit(a) => a.aggregated_from.as_ref(),
            AuditKind::WildcardAudit(a) => a.aggregated_from.as_ref(),
            AuditKind::Trusted(a) => a.aggregated_from.as_ref(),
        }
    }

    /// Pretty-printed hostname part of the URL
    #[must_use]
    pub fn aggregated_from_host(&self) -> Option<&str> {
        self.aggregated_from().iter().find_map(|url| {
            if let Some(gh) = url.strip_prefix("https://raw.githubusercontent.com/") {
                // second slash
                if let Some((pos, _)) = gh.bytes().enumerate().filter(|&(_, c)| c == b'/').nth(1) {
                    return Some(&gh[..pos]);
                }
            }
            let url = url.strip_prefix("https://").unwrap_or(url);
            let url = url.split('/').next()?;

            let known_host = match self.src.github_repo() {
                Some(("google", _)) => Some(".googlesource.com"),
                Some(("mozilla", _)) => Some(".mozilla.org"),
                _ => None,
            };
            if let Some(h) = known_host {
                return Some(url.strip_suffix(h).unwrap_or(url));
            }
            Some(url)
        })
    }

    /// List of authors of this audit. See [cargo_author](https://lib.rs/crates/cargo_author) for parsing.
    #[must_use]
    pub fn who(&self) -> &[String] {
        match self.audit {
            AuditKind::Audit(a) => a.who.as_ref(),
            AuditKind::WildcardAudit(a) => a.who.as_ref(),
            AuditKind::Trusted(a) => a.who.as_ref(),
        }
    }

    /// `Some`, if it's a review of a patch
    #[must_use]
    pub fn delta(&self) -> Option<(VetVersionRef<'_>, VetVersionRef<'_>)> {
        if let AuditKind::Audit(a) = self.audit {
            a.delta()
        } else { None }
    }

    /// Start date of blanket approval
    #[must_use]
    pub fn start(&self) -> Option<&str> {
        match self.audit {
            AuditKind::Audit(_) => None,
            AuditKind::WildcardAudit(a) => Some(a.start.as_str()),
            AuditKind::Trusted(a) => Some(a.start.as_str()),
        }
    }

    /// End date of blanket approval
    #[must_use]
    pub fn end(&self) -> Option<&str> {
        match self.audit {
            AuditKind::Audit(_) => None,
            AuditKind::WildcardAudit(a) => Some(a.end.as_str()),
            AuditKind::Trusted(a) => Some(a.end.as_str()),
        }
    }

    /// If it's a review of complete source code (not just a patch)
    pub fn version(&self) -> Option<VetVersionRef<'_>> {
        match self.audit {
            AuditKind::Audit(a) => {
                a.version.as_deref().map(VetVersionRef::new).or_else(|| a.delta().map(|d| d.1))
            },
            AuditKind::WildcardAudit(_) |
            AuditKind::Trusted(_) => None,
        }
    }

    #[must_use]
    pub fn notes(&self) -> Option<&str> {
        match self.audit {
            AuditKind::Audit(a) => a.notes.as_deref(),
            AuditKind::WildcardAudit(a) => a.notes.as_deref(),
            AuditKind::Trusted(a) => a.notes.as_deref(),
        }
    }

    /// If `Some`, it's reporting malware/vulnerability or something else problematic.
    #[must_use]
    pub fn violation(&self) -> Option<&VersionReq> {
        if let AuditKind::Audit(a) = self.audit {
            a.violation.as_ref()
        } else { None }
    }

    #[must_use] pub fn is_wildcard_trust(&self) -> bool {
        matches!(self.audit, AuditKind::WildcardAudit(_) | AuditKind::Trusted(_))
    }

    #[must_use]
    pub fn audit_type_label(&self) -> &str {
        match self.audit {
            AuditKind::Audit(a) => if a.violation.is_some() { "Rejected" } else { "Audited" },
            AuditKind::WildcardAudit(_) => "Self-approved",
            AuditKind::Trusted(_) => "Trusted without checking",
        }
    }
}

/// Audits can either trust source code, or whole crates, or authors
#[derive(Debug, Clone)]
pub enum AuditKind<'a> {
    /// Specific crate has been reviewed
    Audit(&'a Audit),
    /// YOLO: a crate has been approved without checking
    WildcardAudit(&'a WildcardAudit),
    /// YOLO: a crate author has been allowed to publish anything and have it trusted automatically
    Trusted(&'a Trusted),
}

/// A list of audits from [`fetch_registry_from_url`](MiniVet::fetch_registry_from_url)
#[derive(Debug, Clone)]
pub struct AuditSources {
    pub all: Vec<AuditSource>,
}

impl AuditSources {
    /// Performs linear scan of all sources to find relevant reviews
    #[must_use]
    pub fn for_crate(&self, name: &str) -> Vec<Review<'_>> {
        let mut out = vec![];
        let name = &Ascii::new(name.to_string());

        for src in &self.all {
            if let Some((cname, audits)) = src.audits.audits.get_key_value(name) {
                for audit in audits {
                    out.push(Review {
                        crate_name: cname.0.as_str(),
                        src,
                        audit: AuditKind::Audit(audit),
                    });
                }
            }
            if let Some((cname, waudits)) = src.audits.wildcard_audits.get_key_value(name) {
                for audit in waudits {
                    out.push(Review {
                        crate_name: cname.0.as_str(),
                        src,
                        audit: AuditKind::WildcardAudit(audit),
                    });
                }
            }
        }

        // Unconditionally trusted crates are least useful, so use as last resort
        if out.is_empty() {
            for src in &self.all {
                if let Some((cname, taudits)) = src.audits.trusted.get_key_value(name) {
                    for audit in taudits {
                        out.push(Review {
                            crate_name: cname.0.as_str(),
                            src,
                            audit: AuditKind::Trusted(audit),
                        });
                    }
                }
            }
        }
        out
    }

    /// The `resolve_crates_io_id` callback needs to translate a private numeric ID of crates.io users,
    /// available in crates.io database dump, to a whatever public username you want.
    ///
    /// Removes invalid fields.
    ///
    /// Merges overlapping "safe-to-deploy" delta reviews into full ones.
    pub fn normalized(mut self, resolve_crates_io_id: &(dyn Fn(i64) -> Option<String> + Send + Sync)) -> Self {
        for s in &mut self.all {
            // these are built-in ones and repos are not allowed to override them
            s.audits.criteria.retain(|k, _| k != "safe-to-deploy" && k != "safe-to-run");

            for c in &mut s.audits.criteria.values_mut() {
                // our UI only supports text
                if let (Some(url), None) = (&c.description_url, &c.description) {
                    if url.starts_with("https://") {
                        c.description = Some(format!("[See URL]({url})"));
                    }
                }
            }

            // Using a private crates-io ID is such a bad idea…
            for t in s.audits.trusted.values_mut().flatten() {
                if let StrOrNum::Num(id) = t.user_id {
                    if let Some(s) = resolve_crates_io_id(id) {
                        t.user_id = StrOrNum::Str(s);
                    }
                }
            }
            for t in &mut s.audits.wildcard_audits.values_mut().flatten() {
                if let StrOrNum::Num(id) = t.user_id {
                    if let Some(s) = resolve_crates_io_id(id) {
                        t.user_id = StrOrNum::Str(s);
                    }
                }
            }

            for (CrateName(crate_name), audits) in &mut s.audits.audits.iter_mut() {
                let mut tmp = HashMap::<String, &mut Audit>::default();
                tmp.reserve(audits.len());

                // for some reason there are nearly-dupe reviews that add only one criterion at a time
                for a in &mut *audits {
                    // merge criteria
                    match tmp.entry(a.review_key_without_criteria(crate_name)) {
                        Entry::Occupied(mut dupe) => {
                            let dupe = &mut dupe.get_mut().criteria;
                            // this is quadratic, so don't allow DoS
                            if dupe.len() + a.criteria.len() > 20 { continue; }
                            for c in a.criteria.drain(..) {
                                if !dupe.contains(&c) {
                                    dupe.push(c);
                                }
                            }
                        },
                        Entry::Vacant(e) => {
                            e.insert(a);
                        },
                    }
                }

                // drain() in dedupe emptied these
                audits.retain(|a| !a.criteria.is_empty());

                // TODO: this should be tracking continuity for each criterion separately,
                // but safe-to-deploy is generally the best case, so this as a minimum required should be safe enough.
                let mut has_full_reviews_up_to = audits.iter()
                    .filter(|a| a.delta.is_none() && a.criteria.iter().any(|c| c == "safe-to-deploy"))
                    // there could be other criteria implying safe-to-deploy, but let's keep the merge conservative and only merge obvious cases
                    .filter(|a| a.violation.is_none() && a.delta.is_none() && a.criteria.iter().any(|c| c == "safe-to-deploy"))
                    .filter_map(|a| SemVer::parse(a.version.as_deref()?).ok())
                    .filter(|v| v.pre.is_empty())
                    .max();

                // Merge deltas
                audits.retain_mut(|a| {
                    if let Some((f, t)) = a.delta() {
                        let Ok(f_semver) = SemVer::parse(f.version) else {
                            return false;
                        };
                        let Ok(t_semver) = SemVer::parse(t.version) else {
                            return false;
                        };
                        if t_semver < f_semver {
                            // cargo-vet supports reverse deltas (approving an older version based on newer)
                            // but this is too weird for this implementation.
                            return false;
                        }

                        // if a delta review has a full review as a base, pretend it's a full review too
                        if let Some(max) = &has_full_reviews_up_to {
                            // pre versions are weird to compare. violation obviously not mergeable.
                            let suitable = f_semver.pre.is_empty() && t_semver.pre.is_empty() && a.violation.is_none();
                            let from_overlaps = f_semver < *max || (f_semver == *max && f.git_rev.is_none()); // can't compare git hashes
                            if suitable && from_overlaps && t_semver > *max && a.criteria.iter().any(|c| c == "safe-to-deploy") {
                                debug!("found base for {f} -> {t} in {max} in {crate_name}@{}", s.name);
                                a.version = Some(t.to_string());
                                a.delta = None;
                                has_full_reviews_up_to = Some(t_semver);
                            }
                        }
                    }
                    true
                });
            }
        }
        self
    }
}

impl MiniVet<'static> {
    #[must_use]
    pub fn new() -> Self {
        Self::new_with_client(Cow::Owned(reqwest::Client::builder()
            .user_agent("lib.rs/mini-vet")
            .connect_timeout(Duration::from_secs(4))
            .timeout(Duration::from_secs(10))
            .build().unwrap()))
    }
}

impl<'client> MiniVet<'client> {
    /// Create a new instance of the client
    #[must_use]
    pub fn new_with_client(client: Cow<'client, reqwest::Client>) -> Self {
        Self { client }
    }

    async fn fetch_text(&self, url: &str) -> Result<String, Error> {
        Ok(Box::pin(async move {
            self.client.get(url).send().await?.error_for_status()?.text().await
        }).await?)
    }

    async fn fetch_audits_from_url(&self, url: &str) -> Result<AuditsFile, Error> {
        debug!("fetching vet audit from {url}");

        let data = self.fetch_text(url).await?;
        Ok(toml::from_str(&data).or_else(|e| {
            if let Some(debased) = base64::decode(&data).ok().and_then(|d| String::from_utf8(d).ok()) {
                log::warn!("worked around google vcs mystery base64 for {url}");
                toml::from_str(&debased)
            } else {
                Err(e)
            }
        }).map_err(|e| {
            log::error!("parse error {e} of {url}, data: {:#?}", toml::from_str::<toml::Value>(&data));
            e
        })?)
    }

    /// The registry file doesn't contain audits, only URLs to audit files.
    /// Use with `"https://raw.githubusercontent.com/bholley/cargo-vet/main/registry.toml"` to get the default set.
    ///
    /// Check out [normalized](AuditSources::normalized).
    pub async fn fetch_registry_from_url(&self, url: &str) -> Result<AuditSources, Error> {
        debug!("fetching vet registry from {url}");
        let reg: Registry = {
            let data = self.fetch_text(url).await?;
            toml::from_str(&data)?
        };
        self.fetch_registry(reg).await
    }

    /// Fetches audit files listed in the [`Registry`].
    ///
    /// Check out [normalized](AuditSources::normalized).
    pub async fn fetch_registry(&self, reg: Registry) -> Result<AuditSources, Error> {
        let all = join_all(reg.registry.into_iter().flat_map(|(name, r)| r.url.into_iter().map(move |url| { let name = name.clone(); async move {
            let audits = self.fetch_audits_from_url(&url).await?;
            Ok::<_, Error>(AuditSource {
                name,
                url,
                audits,
            })
        }}))).await.into_iter().collect::<Result<Vec<_>, _>>()?;
        Ok(AuditSources {
            all,
        })
    }
}

#[cfg(test)]
#[tokio::test(flavor = "multi_thread")]
async fn vetload() {
    let _ = env_logger::try_init();
    use std::error::Error;
    fn is_send<T: Send>(v: T) -> T {v}
    let v = is_send(MiniVet::new().fetch_registry_from_url("https://raw.githubusercontent.com/bholley/cargo-vet/main/registry.toml")).await.map_err(|e| {
        log::error!("{:?}", e.source()); e
    }).unwrap();
    assert!(v.all.len() > 4);
    assert!(v.for_crate("Cc").len() >= 5);
}

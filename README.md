# Minimal [cargo-vet](https://lib.rs/cargo-vet) client

This is a library that downloads and parses Rust crate reviews in the [cargo-vet format](https://mozilla.github.io/cargo-vet/recording-audits.html). It can be used to build custom tools for auditing supply-chain security, reusing reviews from [the cargo-vet registry](https://mozilla.github.io/cargo-vet/importing-audits.html), or [indirectly](https://lib.rs/crates/crevette) from [cargo-crev](https://lib.rs/cargo-crev) or Debian or Guix.
